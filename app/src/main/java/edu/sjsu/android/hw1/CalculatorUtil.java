package edu.sjsu.android.hw1;
import java.lang.Math;
import java.util.Formatter;

public class CalculatorUtil {
    //calculates the mortgage with tax
    public static String calculateMortgageWithTax(double amount, int months, double interest) {
        Formatter form = new Formatter();
        if(interest == 0) {
            return form.format("%.2f", ((amount / months) + (0.001 * amount))).toString();
        } else {
            double deciInterest = (interest / 100);
            return form.format("%.2f", ((amount * ((deciInterest / 1200) / (1 - (Math.pow((1 + (deciInterest / 1200)), (0 - months)))))) + (0.001 * amount))).toString();
        }
    }
    //calculates the mortgage without tax
    public static String calculateMortgageWithoutTax(double amount, int months, double interest) {
        Formatter form = new Formatter();
        if(interest == 0) {
            return form.format("%.2f", ((amount / months))).toString();
        } else {
            double deciInterest = (interest / 100);
            return form.format("%.2f", amount * ((deciInterest / 1200) / (1 - (Math.pow((1 + (deciInterest / 1200)), (0 - months)))))).toString();
        }
    }
}
