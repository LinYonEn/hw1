package edu.sjsu.android.hw1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity {

    private EditText text, interest;
    private int change;
    private SeekBar myBar;
    private RadioButton fifteenButton, twentyButton, thirtyButton;
    private CheckBox check;
    private float loan;
    private TextView payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editTextNumber);
        interest = (EditText) findViewById(R.id.editTextNumber1);
        interest = (EditText) findViewById(R.id.editTextNumber1);
        myBar = (SeekBar) findViewById(R.id.seekBar);
        fifteenButton = (RadioButton) findViewById(R.id.radioButton);
        twentyButton = (RadioButton) findViewById(R.id.radioButton1);
        thirtyButton = (RadioButton) findViewById(R.id.radioButton2);
        check = (CheckBox) findViewById(R.id.checkBox);
        payment = (TextView) findViewById(R.id.textView);

        myBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seeker, int value, boolean tf) {
                change = value;
            }

            public void onStartTrackingTouch(SeekBar seeker) {
            }

            public void onStopTrackingTouch(SeekBar seeker) {
                interest.setText(String.valueOf(change));
            }
        });
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                if (text.getText().length() == 0) {
                    Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
                    return;
                }
                loan = Float.parseFloat(text.getText().toString());
                if (loan < 0) {
                    Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
                    return;
                }

                if (check.isChecked()) {
                    if (fifteenButton.isChecked()) {
                        payment.setText("Your monthly payment is $" + String.valueOf(CalculatorUtil.calculateMortgageWithTax(loan, 15, change)) + " per month");
                    } else if (twentyButton.isChecked()) {
                        payment.setText("Your monthly payment is $" + String.valueOf(CalculatorUtil.calculateMortgageWithTax(loan, 20, change)) + " per month");
                    } else {
                        payment.setText("Your monthly payment is $" + String.valueOf(CalculatorUtil.calculateMortgageWithTax(loan, 30, change)) + " per month");
                    }
                } else {
                    if (fifteenButton.isChecked()) {
                        payment.setText("Your monthly payment is $" +String.valueOf(CalculatorUtil.calculateMortgageWithoutTax(loan, 15, change)) + " per month");
                    } else if (twentyButton.isChecked()) {
                        payment.setText("Your monthly payment is $" + String.valueOf(CalculatorUtil.calculateMortgageWithoutTax(loan, 20, change) + " per month"));
                    } else {
                        payment.setText("Your monthly payment is $" +String.valueOf(CalculatorUtil.calculateMortgageWithoutTax(loan, 30, change)) + " per month");
                    }
                }
                break;
        }
    }
}